# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [3.0.2] - 27/10/2023

- Updated `ctools` to version `^4.0`.
- Updated `field_formatter_class` to version `^1.6`.
- Updated `smart_trim` to version `^2.1`.

## [3.0.1] - 07/02/2023
- Re-add `composer.json` in preparation of optimised deployments.
- Update CERN dependencies.

## [3.0.0] - 16/10/2022
- Update module to ensure PHP 8.1 compatibility
- Remove `composer.json`
- Add `.gitignore`

## [2.0.3] - 07/12/2021

- Add `core_version_requirement: ^9 || ^10` and remove `core: 8.x` from composer

## [2.0.2] - 13/10/2021

- Add D9 readiness

## [2.0.1] - 28/05/2021

- Add composer support
- Add CHANGELOG
